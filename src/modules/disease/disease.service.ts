import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateDiseaseDto } from './dto/create-disease.dto';
import { UpdateDiseaseDto } from './dto/update-disease.dto';
import { DiseaseEntity } from './entities/disease.entity';

@Injectable()
export class DiseaseService {
  constructor(
    @InjectRepository(DiseaseEntity)
    private diseaseDb: Repository<DiseaseEntity>,
  ) {}
  async getAll(): Promise<DiseaseEntity[]> {
    return await this.diseaseDb.find();
  }
  async getbyId(id: string): Promise<DiseaseEntity> {
    return await this.diseaseDb.findOne(id).catch((err) => {
      throw new HttpException('disease no found', HttpStatus.NOT_FOUND);
    });
  }
  async create(disease: CreateDiseaseDto): Promise<DiseaseEntity> {
    return await this.diseaseDb.save(disease).catch((err) => {
      throw new HttpException('create disease error', HttpStatus.NOT_FOUND);
    });
  }
  async update(vaccine: UpdateDiseaseDto, id: string): Promise<DiseaseEntity> {
    const dataUpdate = await this.diseaseDb.update(id, vaccine);
    return await this.diseaseDb.findOne(id);
  }
  async delete(id: string): Promise<DiseaseEntity> {
    const vaccine = await this.diseaseDb.findOne(id);
    this.diseaseDb.delete(id);
    return vaccine;
  }
  async getbyIds(ids: any): Promise<DiseaseEntity[]> {
    return await this.diseaseDb.findByIds(ids);
  }
  async foundName(name: string): Promise<DiseaseEntity[]> {
    name = name.toLowerCase();
    return await this.diseaseDb
      .createQueryBuilder('disease')

      .where('disease.name like :name', {
        name: `%${name}%`,
      })
      .select(['disease.name', 'disease.id'])
      .getMany();
  }
}
