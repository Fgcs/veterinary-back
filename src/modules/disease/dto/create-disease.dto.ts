import { IsNotEmpty } from 'class-validator';

export class CreateDiseaseDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  description: string;
}
