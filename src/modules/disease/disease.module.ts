import { Module } from '@nestjs/common';
import { DiseaseService } from './disease.service';
import { DiseaseController } from './disease.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DiseaseEntity } from './entities/disease.entity';

@Module({
  imports: [TypeOrmModule.forFeature([DiseaseEntity])],
  controllers: [DiseaseController],
  providers: [DiseaseService],
  exports: [DiseaseService],
})
export class DiseaseModule {}
