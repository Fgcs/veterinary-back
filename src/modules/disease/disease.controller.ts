import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { DiseaseService } from './disease.service';
import { CreateDiseaseDto } from './dto/create-disease.dto';
import { UpdateDiseaseDto } from './dto/update-disease.dto';
import { DiseaseEntity } from './entities/disease.entity';
@ApiTags('Diseases')
@Controller('api/disease')
export class DiseaseController {
  constructor(private readonly diseaseService: DiseaseService) {}

  @Post()
  create(@Body() createDiseaseDto: CreateDiseaseDto): Promise<DiseaseEntity> {
    return this.diseaseService.create(createDiseaseDto);
  }

  @Get()
  findAll(): Promise<DiseaseEntity[]> {
    return this.diseaseService.getAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<DiseaseEntity> {
    return this.diseaseService.getbyId(id);
  }
  @Get('search/find-by-name')
  async findByEmail(@Query('name') name: string): Promise<DiseaseEntity[]> {
    return await this.diseaseService.foundName(name);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateDiseaseDto: UpdateDiseaseDto,
  ): Promise<DiseaseEntity> {
    return this.diseaseService.update(updateDiseaseDto, id);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<DiseaseEntity> {
    return this.diseaseService.delete(id);
  }
}
