import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { log } from 'console';
import { Repository } from 'typeorm';
import { AddressService } from '../address/address.service';
import { AnimalService } from '../animal/animal.service';
import { UserService } from '../user/user.service';
import { CreateRescueDto } from './dto/create-rescue.dto';
import { UpdateRescueDto } from './dto/update-rescue.dto';
import { RescueEntity } from './entities/rescue.entity';

@Injectable()
export class RescueService {
  constructor(
    @InjectRepository(RescueEntity) private rescuedb: Repository<RescueEntity>,
    private userService: UserService,
    private animalService: AnimalService,
    private addressService: AddressService,
  ) { }
  async create(createRescueDto: CreateRescueDto): Promise<RescueEntity> {

    const { user, animal, address } = createRescueDto;
    try {
      console.log(user.id ? true : false);
      const userDb = user.id
        ? await this.userService.getbyId(user.id)
        : await this.userService.createRescuer(user);
      const animaldb = await this.animalService.create(animal);
      const addressDb = await this.addressService.create(address);
      const rescue: RescueEntity = {
        user: userDb,
        animal: animaldb,
        address: addressDb,
      };
      return this.rescuedb.save(rescue).catch((err) => {
        console.log(err);

        throw new HttpException('create error', HttpStatus.NOT_FOUND);
      });
    } catch (error) {
      console.log(error);
      throw new HttpException('rescues no found', HttpStatus.NOT_FOUND);
    }
  }

  async findAll(): Promise<RescueEntity[]> {
    return await this.rescuedb.find();
  }

  async findOne(id: string): Promise<RescueEntity> {
    return await this.rescuedb.findOne(id).catch((err) => {
      throw new HttpException('rescues no found', HttpStatus.NOT_FOUND);
    });
  }

  async update(
    id: string,
    updateRescueDto: UpdateRescueDto,
  ): Promise<RescueEntity> {
    // const rescue: RescueEntity = {
    //   user: await this.userService.getbyId(updateRescueDto.user),
    //   animal: await this.animalService.getbyId(updateRescueDto.animal),
    //   address: await this.addressService.getbyId(updateRescueDto.address),
    // };
    // const dataUpdate = await this.rescuedb.update(id, rescue);
    // return await this.rescuedb.findOne(id);
    return;
  }

  async remove(id: string): Promise<RescueEntity> {
    const rescue = await this.rescuedb.findOne(id);
    this.rescuedb.delete(id);
    return rescue;
  }
}
