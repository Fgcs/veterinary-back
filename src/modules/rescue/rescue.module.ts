import { forwardRef, Module } from '@nestjs/common';
import { RescueService } from './rescue.service';
import { RescueController } from './rescue.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RescueEntity } from './entities/rescue.entity';
import { UserModule } from '../user/user.module';
import { AnimalModule } from '../animal/animal.module';
import { AddressModule } from '../address/address.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([RescueEntity]),
    forwardRef(() => UserModule),
    forwardRef(() => AnimalModule),
    forwardRef(() => AddressModule),
  ],
  controllers: [RescueController],
  providers: [RescueService],
})
export class RescueModule {}
