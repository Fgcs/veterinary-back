import { IsNotEmpty } from 'class-validator';
import { CreateAddressDto } from 'src/modules/address/dto/create-address.dto';
import { AddressEntity } from 'src/modules/address/entities/address.entity';
import { CreateAnimalDto } from 'src/modules/animal/dto/create-animal.dto';
import { AnimalEntity } from 'src/modules/animal/entities/animal.entity';
import { CreateRescuerDto } from 'src/modules/user/dto/create-rescuer.dto';
import { CreateUserDto } from 'src/modules/user/dto/create-user.dto';
import { UserEntity } from 'src/modules/user/entities/user.entity';

export class CreateRescueDto {
  @IsNotEmpty()
  user: CreateUserDto;
  @IsNotEmpty()
  animal: CreateAnimalDto;
  @IsNotEmpty()
  address: CreateAddressDto;
}
