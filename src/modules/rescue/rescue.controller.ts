import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { RescueService } from './rescue.service';
import { CreateRescueDto } from './dto/create-rescue.dto';
import { UpdateRescueDto } from './dto/update-rescue.dto';
import { RescueEntity } from './entities/rescue.entity';
import { ApiTags } from '@nestjs/swagger';
@ApiTags('rescues')
@Controller('api/rescue')
export class RescueController {
  constructor(private readonly rescueService: RescueService) {}

  @Post()
  create(@Body() createRescueDto: CreateRescueDto): Promise<RescueEntity> {
    return this.rescueService.create(createRescueDto);
  }

  @Get()
  findAll(): Promise<RescueEntity[]> {
    return this.rescueService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<RescueEntity> {
    return this.rescueService.findOne(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateRescueDto: UpdateRescueDto,
  ): Promise<RescueEntity> {
    return this.rescueService.update(id, updateRescueDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<RescueEntity> {
    return this.rescueService.remove(id);
  }
}
