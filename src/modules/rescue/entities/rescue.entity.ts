import { AddressEntity } from 'src/modules/address/entities/address.entity';
import { AnimalEntity } from 'src/modules/animal/entities/animal.entity';
import { UserEntity } from 'src/modules/user/entities/user.entity';
import {
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity('rescue')
export class RescueEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @ManyToOne(() => UserEntity, (user) => user.id)
  user: UserEntity;
  @ManyToOne(() => AnimalEntity, (animal) => animal.id,
    {
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    })
  animal: AnimalEntity;
  @ManyToOne(() => AddressEntity, (address) => address.id,
    {
      onDelete: 'CASCADE',
      onUpdate: 'CASCADE',
    })
  address: AddressEntity;

  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_at?: Date;
  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_at?: Date;
}
