import { IsNotEmpty, IsOptional } from 'class-validator';
export class CreateAddressDto {
  @IsOptional()
  street: string;
  @IsOptional()
  city: string;
  @IsOptional()
  state: string;
  @IsOptional()
  postal_code: string;
  @IsOptional()
  home: string;
  @IsOptional()
  reference: string;

  @IsOptional()
  location: {
    latitude: string;
    longitude: string;
  };
}
