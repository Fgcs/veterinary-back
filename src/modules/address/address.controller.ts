import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AddressService } from './address.service';
import { CreateAddressDto } from './dto/create-address.dto';
import { UpdateAddressDto } from './dto/update-address.dto';
import { AddressEntity } from './entities/address.entity';
@ApiTags('address')
@Controller('api/address')
export class AddressController {
  constructor(private readonly addressService: AddressService) {}

  @Post()
  create(@Body() createAddressDto: CreateAddressDto): Promise<AddressEntity> {
    return this.addressService.create(createAddressDto);
  }

  @Get()
  findAll(): Promise<AddressEntity[]> {
    return this.addressService.getAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<AddressEntity> {
    return this.addressService.getbyId(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateAddressDto: UpdateAddressDto,
  ): Promise<AddressEntity> {
    return this.addressService.update(updateAddressDto, id);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<AddressEntity> {
    return this.addressService.delete(id);
  }
}
