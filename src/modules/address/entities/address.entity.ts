import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import { LocationEntity } from './location.entity';
@Entity('address')
export class AddressEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  street: string;
  @Column()
  city: string;
  @Column()
  state: string;
  @Column()
  postal_code: string;
  @Column()
  home: string;
  @Column({ default: '' })
  reference: string;
  @OneToOne(() => LocationEntity)
  @JoinColumn()
  location: LocationEntity;
  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;
  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_at: Date;
}
