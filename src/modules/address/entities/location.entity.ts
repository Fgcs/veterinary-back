import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
@Entity('location')
export class LocationEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column()
  latitude: string;
  @Column()
  longitude: string;
}
