import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateAddressDto } from './dto/create-address.dto';
import { UpdateAddressDto } from './dto/update-address.dto';
import { AddressEntity } from './entities/address.entity';
import { LocationEntity } from './entities/location.entity';

@Injectable()
export class AddressService {
  constructor(
    @InjectRepository(AddressEntity)
    private addressDB: Repository<AddressEntity>,
    @InjectRepository(LocationEntity)
    private locationdb: Repository<LocationEntity>,
  ) { }
  async getAll(): Promise<AddressEntity[]> {
    return await this.addressDB.find({ relations: ['location'] });
  }
  async getbyId(id: string): Promise<AddressEntity> {
    return await this.addressDB
      .findOne(id, { relations: ['location'] })
      .catch((err) => {
        throw new HttpException('address no found', HttpStatus.NOT_FOUND);
      });
  }
  async create(address: CreateAddressDto): Promise<AddressEntity> {
    if (address.location) {
      const location = await this.saveLocation(address.location);
      address.location = location;
    } else {
      const body = {
        latitude: '0',
        longitude: '0',
      };
      const location = await this.saveLocation(body);
      address.location = location;
    }

    return await this.addressDB.save(address).catch((err) => {
      throw new HttpException('create address error', HttpStatus.NOT_FOUND);
    });
  }
  async update(address: UpdateAddressDto, id: string): Promise<AddressEntity> {
    const dataUpdate = await this.addressDB.update(id, address);
    return await this.addressDB.findOne(id);
  }
  async delete(id: string): Promise<AddressEntity> {
    const address = await this.addressDB.findOne(id);
    this.addressDB.delete(id);
    return address;
  }
  async saveLocation(location: LocationEntity) {
    return this.locationdb.save(location);
  }
}
