import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { BreedService } from './breed.service';
import { CreateBreedDto } from './dto/create-breed.dto';
import { UpdateBreedDto } from './dto/update-breed.dto';
import { BreedEntity } from './entities/breed.entity';
@ApiTags('Breeds')
@Controller('api/breed')
export class BreedController {
  constructor(private readonly breedService: BreedService) {}

  @Post()
  create(@Body() createBreedDto: CreateBreedDto): Promise<BreedEntity> {
    return this.breedService.create(createBreedDto);
  }

  @Get()
  findAll(): Promise<BreedEntity[]> {
    return this.breedService.getAll();
  }
  @Get('/find-by-name')
  async findByEmail(@Query('name') name: string) {
    return await this.breedService.foundName(name);
  }
  @Get(':id')
  findOne(@Param('id') id: string): Promise<BreedEntity> {
    return this.breedService.getbyId(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBreedDto: UpdateBreedDto,
  ): Promise<BreedEntity> {
    return this.breedService.update(updateBreedDto, id);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<BreedEntity> {
    return this.breedService.delete(id);
  }
}
