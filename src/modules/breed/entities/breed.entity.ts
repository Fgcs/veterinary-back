import { AnimalEntity } from 'src/modules/animal/entities/animal.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';

@Entity('breed')
export class BreedEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column()
  @Unique(['name'])
  name: string;
  @OneToMany(() => AnimalEntity, (animal) => animal.breed, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  animals: AnimalEntity[];
  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;
  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_at: Date;
}
