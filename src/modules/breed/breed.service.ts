import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateBreedDto } from './dto/create-breed.dto';
import { UpdateBreedDto } from './dto/update-breed.dto';
import { BreedEntity } from './entities/breed.entity';

@Injectable()
export class BreedService {
  constructor(
    @InjectRepository(BreedEntity)
    private breedDB: Repository<BreedEntity>,
  ) {}
  async getAll(): Promise<BreedEntity[]> {
    return await this.breedDB.find();
  }
  async getbyId(id: string): Promise<BreedEntity> {
    return await this.breedDB.findOne(id).catch((err) => {
      throw new HttpException('breed no found', HttpStatus.NOT_FOUND);
    });
  }
  async create(breed: CreateBreedDto): Promise<BreedEntity> {
    breed.name = breed.name.toLowerCase().trim();
    return await this.breedDB.save(breed).catch((err) => {
      console.log(err);

      throw new HttpException('create specie error', HttpStatus.NOT_FOUND);
    });
  }
  async update(breed: UpdateBreedDto, id: string): Promise<BreedEntity> {
    breed.name = breed.name.toLowerCase().trim();
    const dataUpdate = await this.breedDB.update(id, breed);
    return await this.breedDB.findOne(id);
  }
  async delete(id: string): Promise<BreedEntity> {
    const breed = await this.breedDB.findOne(id);
    this.breedDB.delete(id);
    return breed;
  }
  async foundName(name: string): Promise<BreedEntity[]> {
    name = name.toLowerCase();
    return await this.breedDB
      .createQueryBuilder('breed')
      .where('breed.name like :name', {
        name: `%${name}%`,
      })
      .select(['breed.name', 'breed.id'])
      .getMany();
    // return await this.breedDB.find({
    //   select: ['id', 'name'],
    //   where: [`%${name}%`],
    // });
  }
}
