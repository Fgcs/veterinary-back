import { AnimalEntity } from 'src/modules/animal/entities/animal.entity';
import { CareEntity } from 'src/modules/care/entities/care.entity';
import { DiseaseEntity } from 'src/modules/disease/entities/disease.entity';
import { UserEntity } from 'src/modules/user/entities/user.entity';
import { VaccineEntity } from 'src/modules/vaccine/entities/vaccine.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity('consultation')
export class ConsultationEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column()
  record?: string;
  @Column()
  date: Date;
  @Column({ default: '' })
  observations: string;
  @Column()
  weight: number;
  @ManyToOne(() => UserEntity, (doctor) => doctor.id, { onDelete: 'CASCADE' })
  doctor: UserEntity;
  @ManyToOne(() => AnimalEntity, (animal) => animal.id)
  animal: AnimalEntity;
  @ManyToMany(() => VaccineEntity, (vaccine) => vaccine.id, {
    cascade: true,
  })
  @JoinTable()
  vaccines: VaccineEntity[];
  @ManyToMany(() => DiseaseEntity, (disease) => disease.id, {
    cascade: true,
  })
  @JoinTable()
  diseases: DiseaseEntity[];
  @ManyToMany(() => CareEntity, (care) => care.id, {
    cascade: true,
  })
  @JoinTable()
  cares: CareEntity[];
  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_at?: Date;
  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_at?: Date;
}
