import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ConsultationService } from './consultation.service';
import { CreateConsultationDto } from './dto/create-consultation.dto';
import { UpdateConsultationDto } from './dto/update-consultation.dto';
import { ConsultationEntity } from './entities/consultation.entity';
@ApiTags('Consultations')
@Controller('api/consultation')
export class ConsultationController {
  constructor(private readonly consultationService: ConsultationService) {}

  @Post()
  create(
    @Body() createConsultationDto: CreateConsultationDto,
  ): Promise<ConsultationEntity> {
    return this.consultationService.create(createConsultationDto);
  }

  @Get()
  findAll(): Promise<ConsultationEntity[]> {
    return this.consultationService.getAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<ConsultationEntity> {
    return this.consultationService.getbyId(id);
  }
  @Get('animal/:id')
  getByAnimal(@Param('id') id: string): Promise<ConsultationEntity[]> {
    return this.consultationService.getByAnimal(id);
  }
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateConsultationDto: UpdateConsultationDto,
  ): Promise<ConsultationEntity> {
    return this.consultationService.update(updateConsultationDto, id);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<ConsultationEntity> {
    return this.consultationService.delete(id);
  }
}
