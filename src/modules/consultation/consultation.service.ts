import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { skuGenerate } from 'src/util/sku-generate';
import { Repository } from 'typeorm';
import { AnimalService } from '../animal/animal.service';
import { CareService } from '../care/care.service';
import { DiseaseService } from '../disease/disease.service';
import { UserService } from '../user/user.service';
import { VaccineService } from '../vaccine/vaccine.service';
import { CreateConsultationDto } from './dto/create-consultation.dto';
import { UpdateConsultationDto } from './dto/update-consultation.dto';
import { ConsultationEntity } from './entities/consultation.entity';

@Injectable()
export class ConsultationService {
  constructor(
    @InjectRepository(ConsultationEntity)
    private database: Repository<ConsultationEntity>,
    private userService: UserService,
    private animalService: AnimalService,
    private vaccinService: VaccineService,
    private diseaseService: DiseaseService,
    private careService: CareService,
  ) {}
  async create(req: CreateConsultationDto): Promise<ConsultationEntity> {
    const consultation: ConsultationEntity = {
      date: req.date,
      record: skuGenerate(),
      weight: req.weight,
      observations: req.observations,
      animal: await this.animalService.getbyId(req.animalId),
      doctor: await this.userService.getbyId(req.doctorId),
      cares: await this.careService.getbyIds(req.cares),
      diseases: await this.diseaseService.getbyIds(req.diseases),
      vaccines: await this.vaccinService.getbyIds(req.vaccines),
    };
    return this.database.save(consultation).catch((err) => {
      throw new HttpException('create disease error', HttpStatus.NOT_FOUND);
    });
  }

  async getAll(): Promise<ConsultationEntity[]> {
    return await this.database.find();
  }
  async getbyId(id: string): Promise<ConsultationEntity> {
    return await this.database.findOne(id).catch((err) => {
      throw new HttpException('consultation no found', HttpStatus.NOT_FOUND);
    });
  }

  async update(
    vaccine: UpdateConsultationDto,
    id: string,
  ): Promise<ConsultationEntity> {
    const dataUpdate = await this.database.update(id, vaccine);
    return await this.database.findOne(id);
  }
  async delete(id: string): Promise<ConsultationEntity> {
    const vaccine = await this.database.findOne(id);
    this.database.delete(id);
    return vaccine;
  }
  async getByAnimal(animal: string): Promise<ConsultationEntity[]> {
    return await this.database.find({
      relations: ['vaccines', 'diseases', 'cares', 'doctor'],
      where: [{ animal: { id: animal } }],
    });
  }
}
