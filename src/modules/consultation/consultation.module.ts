import { forwardRef, Module } from '@nestjs/common';
import { ConsultationService } from './consultation.service';
import { ConsultationController } from './consultation.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConsultationEntity } from './entities/consultation.entity';
import { UserModule } from '../user/user.module';
import { AnimalModule } from '../animal/animal.module';
import { CareModule } from '../care/care.module';
import { VaccineModule } from '../vaccine/vaccine.module';
import { DiseaseModule } from '../disease/disease.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([ConsultationEntity]),
    forwardRef(() => UserModule),
    forwardRef(() => AnimalModule),
    forwardRef(() => CareModule),
    forwardRef(() => VaccineModule),
    forwardRef(() => DiseaseModule),
  ],
  controllers: [ConsultationController],
  providers: [ConsultationService],
})
export class ConsultationModule {}
