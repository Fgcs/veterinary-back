import { IsArray, IsNotEmpty, IsOptional } from 'class-validator';
export class CreateConsultationDto {
  date: Date;
  @IsNotEmpty()
  weight: number;
  @IsNotEmpty()
  observations: string;
  @IsNotEmpty()
  doctorId: string;
  @IsNotEmpty()
  animalId: string;
  @IsArray()
  vaccines: [];
  @IsArray()
  diseases: [];
  @IsArray()
  cares: [];
}
