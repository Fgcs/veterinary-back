import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CareService } from './care.service';
import { CreateCareDto } from './dto/create-care.dto';
import { UpdateCareDto } from './dto/update-care.dto';
import { CareEntity } from './entities/care.entity';
@ApiTags('Cares')
@Controller('api/care')
export class CareController {
  constructor(private readonly careService: CareService) {}

  @Post()
  create(@Body() createCareDto: CreateCareDto): Promise<CareEntity> {
    return this.careService.create(createCareDto);
  }

  @Get()
  findAll(): Promise<CareEntity[]> {
    return this.careService.getAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<CareEntity> {
    return this.careService.getbyId(id);
  }
  @Get('search/find-by-name')
  async findByEmail(@Query('name') name: string): Promise<CareEntity[]> {
    return await this.careService.foundName(name);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCareDto: UpdateCareDto,
  ): Promise<CareEntity> {
    return this.careService.update(updateCareDto, id);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<CareEntity> {
    return this.careService.delete(id);
  }
}
