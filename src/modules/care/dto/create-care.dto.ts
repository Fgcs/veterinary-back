import { IsNotEmpty } from 'class-validator';
export class CreateCareDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  description: string;
}
