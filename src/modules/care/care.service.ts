import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCareDto } from './dto/create-care.dto';
import { UpdateCareDto } from './dto/update-care.dto';
import { CareEntity } from './entities/care.entity';

@Injectable()
export class CareService {
  constructor(
    @InjectRepository(CareEntity) private caredb: Repository<CareEntity>,
  ) {}
  async getAll(): Promise<CareEntity[]> {
    return await this.caredb.find();
  }
  async getbyId(id: string): Promise<CareEntity> {
    return await this.caredb.findOne(id).catch((err) => {
      throw new HttpException('care  no found', HttpStatus.NOT_FOUND);
    });
  }
  async create(care: CreateCareDto): Promise<CareEntity> {
    care.name = care.name.toLowerCase().trim();
    return await this.caredb.save(care).catch((err) => {
      throw new HttpException('create care error', HttpStatus.NOT_FOUND);
    });
  }
  async update(care: UpdateCareDto, id: string): Promise<CareEntity> {
    care.name = care.name.toLowerCase().trim();
    const dataUpdate = await this.caredb.update(id, care);
    return await this.caredb.findOne(id);
  }
  async delete(id: string): Promise<CareEntity> {
    const care = await this.caredb.findOne(id);
    this.caredb.delete(id);
    return care;
  }
  async getbyIds(ids: any): Promise<CareEntity[]> {
    return await this.caredb.findByIds(ids);
  }
  async foundName(name: string): Promise<CareEntity[]> {
    name = name.toLowerCase();
    return await this.caredb
      .createQueryBuilder('care')

      .where('care.name like :name', {
        name: `%${name}%`,
      })
      .select(['care.name', 'care.id'])
      .getMany();
  }
}
