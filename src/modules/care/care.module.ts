import { Module } from '@nestjs/common';
import { CareService } from './care.service';
import { CareController } from './care.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CareEntity } from './entities/care.entity';

@Module({
  imports: [TypeOrmModule.forFeature([CareEntity])],
  controllers: [CareController],
  providers: [CareService],
  exports: [CareService],
})
export class CareModule {}
