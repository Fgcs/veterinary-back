import { getRepository } from 'typeorm';
import { ConfigService } from '@nestjs/config';
import { hashSync } from 'bcryptjs';
import { UserEntity } from '../entities/user.entity';
export const setDefaultUser = async (config: ConfigService) => {
  const userRepository = getRepository<UserEntity>(UserEntity);
  try {
    const defaultUser = await userRepository
      .createQueryBuilder()
      .where('email = :email', {
        email: config.get<string>('DEFAULT_USER_EMAIL'),
      })
      .getOne();
    if (!defaultUser) {
      const adminUser = userRepository.create({
        email: config.get('DEFAULT_USER_EMAIL'),
        password: hashSync(config.get('DEFAULT_USER_PASSWORD'), 10),
        rol: 'admin',
        name: 'ADMIN',
        lastName: 'ADMIN',
        username: 'Admin',
        dni: 'Admin',
        phone: '124567',
      });

      return await userRepository.save(adminUser);
    }
  } catch (err) {
    console.log(err);
  }
};
