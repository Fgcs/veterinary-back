import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';
import { hashSync } from 'bcryptjs';
import { CreateRescuerDto } from './dto/create-rescuer.dto';
@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity) private userDb: Repository<UserEntity>,
  ) { }
  async create(user: CreateUserDto): Promise<UserEntity> {
    return await this.userDb.save(this.mappUser(user)).catch((err) => {
      throw new HttpException('create user error', HttpStatus.NOT_FOUND);
    });
  }
  async createRescuer(rescuer: CreateRescuerDto): Promise<UserEntity> {
    const rescuerBody = {
      rol: 'rescuer',
      name: rescuer.name,
      lastName: rescuer.lastName,
      dni: rescuer.dni,
      phone: rescuer.phone,
      email: rescuer.email,
    };

    return this.userDb.save(rescuerBody).catch((err) => {
      console.log(err);

      throw new HttpException('create rescuer error', HttpStatus.NOT_FOUND);
    });
  }
  async getAll(): Promise<UserEntity[]> {
    return await this.userDb.find();
  }
  async getbyId(id: string): Promise<UserEntity> {
    return await this.userDb.findOne(id).catch((err) => {
      throw new HttpException('user no found', HttpStatus.NOT_FOUND);
    });
  }
  async getByDni(dni: string): Promise<UserEntity[]> {
    return await this.userDb
      .createQueryBuilder('user')
      .where('user.dni like :dni', {
        dni: `%${dni}%`,
      })
      .select([
        'user.name',
        'user.id',
        'user.email',
        'user.phone',
        'user.lastName',
        'user.dni',
      ])
      .getMany();
  }
  async getByUsername(username: string): Promise<UserEntity[]> {
    return await this.userDb
      .createQueryBuilder('user')
      .where('user.username like :username', {
        username: `%${username}%`,
      })
      .select([
        'user.name',
        'user.id',
        'user.email',
        'user.phone',
        'user.lastName',
        'user.dni',
        'user.username',
      ])
      .getMany();
  }
  async login(email: string): Promise<UserEntity> {
    return await this.userDb
      .findOne(
        { email: email },
        {
          select: [
            'id',
            'email',
            'password',
            'rol',
            'lastName',
            'dni',
            'phone',
          ],
        },
      )
      .catch((err) => {
        throw new HttpException(
          'User with this email does not exist',
          HttpStatus.NOT_FOUND,
        );
      });
  }
  async getByEmail(email: string): Promise<UserEntity> {
    return await this.userDb.findOne({ email: email }).catch((err) => {
      throw new HttpException(
        'User with this email does not exist',
        HttpStatus.NOT_FOUND,
      );
    });
  }
  async update(user: UpdateUserDto, id: string): Promise<UserEntity> {
    const dataUpdate = await this.userDb.update(id, user);
    return await this.userDb.findOne(id);
  }
  async delete(id: string): Promise<UserEntity> {
    const user = await this.userDb.findOne(id);
    await this.userDb.delete(id);
    return user;
  }
  private mappUser(user: CreateUserDto): UserEntity {
    return {
      name: user.name.toLowerCase(),
      lastName: user.lastName.toLowerCase(),
      email: user.email.toLowerCase(),
      phone: user.phone ? user.phone : 'no tiene',
      username: user.username.toLowerCase(),
      password: hashSync(user.password, 10),
      dni: user.dni,
    };
  }
}
