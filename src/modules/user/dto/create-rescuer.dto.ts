import { IsNotEmpty, IsOptional } from 'class-validator';
export class CreateRescuerDto {
  @IsNotEmpty({
    message: 'El nombre del usuario es requerido',
  })
  name: string;
  @IsNotEmpty({
    message: 'El apellido del usuario es requerido',
  })
  lastName: string;
  @IsNotEmpty({
    message: 'la cedula del usuario es requerido',
  })
  dni: string;
  @IsOptional()
  phone: string;
  @IsOptional()
  email: string;
  //   @IsNotEmpty({
  //     message: 'El alias o username del usuario es requerido',
  //   })
  //   username: string;
  //   @IsNotEmpty({
  //     message: 'la contraseña del usuario es requerido',
  //   })
  //   password: string;
}
