import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  Put,
} from '@nestjs/common';
import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';
import { ApiTags } from '@nestjs/swagger';
import { CreateRescuerDto } from './dto/create-rescuer.dto';
@ApiTags('Users')
@Controller('api/user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post()
  create(@Body() createUserDto: CreateUserDto): Promise<UserEntity> {
    return this.userService.create(createUserDto);
  }
  @Post('/rescuer')
  createRescuer(@Body() rescuer: CreateRescuerDto): Promise<UserEntity> {
    return this.userService.createRescuer(rescuer);
  }
  @Get()
  findAll(): Promise<UserEntity[]> {
    return this.userService.getAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<UserEntity> {
    return this.userService.getbyId(id);
  }
  @Get('email/:email')
  getByEmail(@Param('email') email: string): Promise<UserEntity> {
    return this.userService.getByEmail(email);
  }
  @Get('search/find-by-dni')
  getByDni(@Query('dni') dni: string): Promise<UserEntity[]> {
    return this.userService.getByDni(dni);
  }
  @Get('search/find-by-username')
  getByUsername(@Query('username') username: string): Promise<UserEntity[]> {
    return this.userService.getByUsername(username);
  }
  @Put(':id')
  update(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<UserEntity> {
    return this.userService.update(updateUserDto, id);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<UserEntity> {
    return this.userService.delete(id);
  }
}
