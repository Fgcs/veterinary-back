import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column({ default: '' })
  name: string;
  @Column()
  lastName: string;
  @Column()
  @Unique(['dni'])
  dni: string;
  @Column()
  phone: string;
  @Column({
    type: 'enum',
    enum: ['doctor', 'worker', 'assitent', 'user', 'admin', 'rescuer'],
    default: 'user',
  })
  rol?: string;
  @Column({ nullable: true })
  @Unique(['email'])
  email: string;
  @Column({ nullable: true })
  @Unique(['username'])
  username: string;
  @Column({ select: false, nullable: true })
  password: string;
  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_at?: Date;
  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_at?: Date;
}
