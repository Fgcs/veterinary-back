import { Module } from '@nestjs/common';
import { AddressModule } from './address/address.module';
import { BreedModule } from './breed/breed.module';
import { SpecieModule } from './specie/specie.module';
import { VaccineModule } from './vaccine/vaccine.module';
import { DiseaseModule } from './disease/disease.module';
import { CareModule } from './care/care.module';
import { AnimalModule } from './animal/animal.module';
import { UserModule } from './user/user.module';
import { ConsultationModule } from './consultation/consultation.module';
import { DatabaseModule } from './database/database.module';
import { RescueModule } from './rescue/rescue.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    SpecieModule,
    BreedModule,
    AddressModule,
    VaccineModule,
    DiseaseModule,
    CareModule,
    AnimalModule,
    UserModule,
    ConsultationModule,
    DatabaseModule,
    RescueModule,
    AuthModule,
  ],
})
export class ModulesModule {}
