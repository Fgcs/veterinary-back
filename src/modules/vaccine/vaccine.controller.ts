import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { VaccineService } from './vaccine.service';
import { CreateVaccineDto } from './dto/create-vaccine.dto';
import { UpdateVaccineDto } from './dto/update-vaccine.dto';
import { VaccineEntity } from './entities/vaccine.entity';
import { ApiTags } from '@nestjs/swagger';
@ApiTags('Vaccines')
@Controller('api/vaccine')
export class VaccineController {
  constructor(private readonly vaccineService: VaccineService) {}

  @Post()
  create(@Body() createVaccineDto: CreateVaccineDto): Promise<VaccineEntity> {
    return this.vaccineService.create(createVaccineDto);
  }

  @Get()
  findAll(): Promise<VaccineEntity[]> {
    return this.vaccineService.getAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<VaccineEntity> {
    return this.vaccineService.getbyId(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateVaccineDto: UpdateVaccineDto,
  ): Promise<VaccineEntity> {
    return this.vaccineService.update(updateVaccineDto, id);
  }
  @Get('search/find-by-name')
  async findByEmail(@Query('name') name: string): Promise<VaccineEntity[]> {
    return await this.vaccineService.foundName(name);
  }
  @Delete(':id')
  remove(@Param('id') id: string): Promise<VaccineEntity> {
    return this.vaccineService.delete(id);
  }
}
