import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateVaccineDto } from './dto/create-vaccine.dto';
import { UpdateVaccineDto } from './dto/update-vaccine.dto';
import { VaccineEntity } from './entities/vaccine.entity';

@Injectable()
export class VaccineService {
  constructor(
    @InjectRepository(VaccineEntity)
    private vaccineDb: Repository<VaccineEntity>,
  ) {}
  async getAll(): Promise<VaccineEntity[]> {
    return await this.vaccineDb.find();
  }
  async getbyId(id: string): Promise<VaccineEntity> {
    return await this.vaccineDb.findOne(id).catch((err) => {
      throw new HttpException('vaccine no found', HttpStatus.NOT_FOUND);
    });
  }
  async create(vaccine: CreateVaccineDto): Promise<VaccineEntity> {
    return await this.vaccineDb.save(vaccine).catch((err) => {
      throw new HttpException('create vaccine error', HttpStatus.NOT_FOUND);
    });
  }
  async update(vaccine: UpdateVaccineDto, id: string): Promise<VaccineEntity> {
    const dataUpdate = await this.vaccineDb.update(id, vaccine);
    return await this.vaccineDb.findOne(id);
  }
  async delete(id: string): Promise<VaccineEntity> {
    const vaccine = await this.vaccineDb.findOne(id);
    this.vaccineDb.delete(id);
    return vaccine;
  }
  async getbyIds(ids: any): Promise<VaccineEntity[]> {
    return await this.vaccineDb.findByIds(ids);
  }
  async foundName(name: string): Promise<VaccineEntity[]> {
    name = name.toLowerCase();
    return await this.vaccineDb
      .createQueryBuilder('vaccine')

      .where('vaccine.name like :name', {
        name: `%${name}%`,
      })
      .select(['vaccine.name', 'vaccine.id'])
      .getMany();
  }
}
