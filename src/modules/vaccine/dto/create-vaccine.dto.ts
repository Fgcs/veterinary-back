import { IsNotEmpty, IsOptional } from 'class-validator';
export class CreateVaccineDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  description: string;
  @IsNotEmpty()
  manufacturer: string;
  @IsNotEmpty()
  via: string;
}
