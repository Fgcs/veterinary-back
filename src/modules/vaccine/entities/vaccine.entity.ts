import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
@Entity('vaccine')
export class VaccineEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;
  @Column({ default: '' })
  name: string;
  @Column()
  description: string;
  @Column()
  manufacturer: string;
  @Column()
  via: string;
  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_at: Date;
  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_at: Date;
}
