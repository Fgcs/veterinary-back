import {
  HttpException,
  HttpStatus,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { compareSync, hashSync } from 'bcryptjs';
import { RegisterDto } from './dto/register.dto';
import { AuthLoginDto } from './dto/auth-login.dto';
import { AuthEntity } from './entities/auth.entity';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(AuthEntity)
    private AuthDB: Repository<AuthEntity>,
    private usersService: UserService,
    private jwtService: JwtService,
  ) {}

  async register(user: RegisterDto) {
    user.email = user.email.toLowerCase();
    user.lastName = user.lastName.toLowerCase();
    user.password = hashSync(user.password, 10);
    try {
      const createdUser = await this.usersService.create({
        ...user,
      });
      createdUser.password = undefined;
      return createdUser;
    } catch (error) {
      throw new HttpException(
        'Something went wrong',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
  async login(auth: AuthLoginDto): Promise<AuthEntity> {
    const { email, password } = auth;
    const userDB = await this.usersService.login(email);
    if (!userDB) throw new NotFoundException('User not found');
    const matchPassword = compareSync(password, userDB.password);

    if (!matchPassword) throw new UnauthorizedException('Password Incorrect');
    // if (userDB.status === 'inactive')
    //   throw new UnauthorizedException('User Inactive');
    const payload = {
      id: String(userDB.id),
      email: userDB.email,
    };
    userDB.password = undefined;
    const expiresTime = 24 * 60 * 60;
    const body: AuthEntity = {
      tokenType: 'Bearer',
      token: this.jwtService.sign(payload, { expiresIn: '1d' }),
      expiresIn: expiresTime,
      user: userDB,
    };
    return await this.AuthDB.save(body).catch((err) => {
      throw new HttpException('auth login error', HttpStatus.NOT_FOUND);
    });
  }
  async validateUser(username: string, pass: string): Promise<any> {
    const user = await this.usersService.getbyId(username);
    if (user && user.password === pass) {
      const { password, ...result } = user;
      return result;
    }
    return null;
  }
}
