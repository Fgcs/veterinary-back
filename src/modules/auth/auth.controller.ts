import { Controller, Post, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { AuthLoginDto } from './dto/auth-login.dto';
import { RegisterDto } from './dto/register.dto';
import { AuthEntity } from './entities/auth.entity';
@ApiTags('Auth')
@Controller('api/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/register')
  async register(@Body() newUser: RegisterDto) {
    return await this.authService.register(newUser);
  }
  @Post('/login')
  async login(@Body() auth: AuthLoginDto): Promise<AuthEntity> {
    return await this.authService.login(auth);
  }
}
