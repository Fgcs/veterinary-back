import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AnimalService } from './animal.service';
import { CreateAnimalDto } from './dto/create-animal.dto';
import { UpdateAnimalDto } from './dto/update-animal.dto';
import { AnimalEntity } from './entities/animal.entity';
@ApiTags('Animals')
@Controller('api/animal')
export class AnimalController {
  constructor(private readonly animalService: AnimalService) {}

  @Post()
  async create(
    @Body() createAnimalDto: CreateAnimalDto,
  ): Promise<AnimalEntity> {
    return this.animalService.create(createAnimalDto);
  }

  @Get()
  async findAll(): Promise<AnimalEntity[]> {
    return this.animalService.getAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<AnimalEntity> {
    return this.animalService.getbyId(id);
  }
  @Get('specie/:specieId')
  async getBySpecie(@Param('specieId') id: string): Promise<AnimalEntity[]> {
    return this.animalService.getBySpecie(id);
  }
  @Get('breed/:breedId')
  async getByBreed(@Param('breedId') id: string): Promise<AnimalEntity[]> {
    return this.animalService.getByBreed(id);
  }
  @Patch(':id')
  async update(
    @Param('id') id: string,
    @Body() updateAnimalDto: UpdateAnimalDto,
  ): Promise<AnimalEntity> {
    return this.animalService.update(updateAnimalDto, id);
  }

  @Delete(':id')
  async remove(@Param('id') id: string): Promise<AnimalEntity> {
    return this.animalService.delete(id);
  }
}
