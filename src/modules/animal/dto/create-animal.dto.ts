import { IsNotEmpty, IsOptional } from 'class-validator';
export class CreateAnimalDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  weight: number;
  @IsNotEmpty()
  gender: string;
  @IsOptional()
  hair_color: string;
  @IsOptional()
  birthday: Date;
  @IsNotEmpty()
  breedId: string;
  @IsNotEmpty()
  specieId: string;
}
