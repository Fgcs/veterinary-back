import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { skuGenerate } from 'src/util/sku-generate';
import { Repository } from 'typeorm';
import { BreedService } from '../breed/breed.service';
import { SpecieService } from '../specie/specie.service';
import { CreateAnimalDto } from './dto/create-animal.dto';
import { UpdateAnimalDto } from './dto/update-animal.dto';
import { AnimalEntity } from './entities/animal.entity';

@Injectable()
export class AnimalService {
  constructor(
    @InjectRepository(AnimalEntity) private animalDb: Repository<AnimalEntity>,
    private breedService: BreedService,
    private specieService: SpecieService,
  ) { }
  async create(animal: CreateAnimalDto): Promise<AnimalEntity> {
    const breed = await this.breedService.getbyId(animal.breedId);
    const specie = await this.specieService.getbyId(animal.specieId);
    const newAnimal: AnimalEntity = {
      ...animal,
      specie,
      breed,
      record: skuGenerate(),
    };
    return await this.animalDb.save(newAnimal).catch((err) => {
      console.log(err);

      throw new HttpException('create animal error', HttpStatus.NOT_FOUND);
    });
  }
  async getAll(): Promise<AnimalEntity[]> {
    return await this.animalDb.find();
  }
  async getbyId(id: string): Promise<AnimalEntity> {
    return await this.animalDb
      .findOne(id, { relations: ['specie', 'breed'] })
      .catch((err) => {
        throw new HttpException('animal no found', HttpStatus.NOT_FOUND);
      });
  }
  async update(animal: UpdateAnimalDto, id: string): Promise<AnimalEntity> {
    const dataUpdate = await this.animalDb.update(id, animal);
    return await this.animalDb.findOne(id);
  }
  async delete(id: string): Promise<AnimalEntity> {
    try {
      const specie = await this.animalDb.findOne(id);
      this.animalDb.delete(id);
      return specie;
    } catch (error) {
      console.log(error);
    }
  }
  async getBySpecie(specie: string): Promise<AnimalEntity[]> {
    return await this.animalDb
      .createQueryBuilder('animal')
      .where('animal.specie = :id', { id: specie })
      .getMany();
  }
  async getByBreed(breed: string): Promise<AnimalEntity[]> {
    return await this.animalDb
      .createQueryBuilder('animal')
      .where('animal.breed = :id', { id: breed })
      .getMany();
  }
  async getByRecord(record: string): Promise<AnimalEntity> {
    return this.animalDb.findOne({ record: record }).catch((err) => {
      throw new HttpException('animal no found', HttpStatus.NOT_FOUND);
    });
  }
}
