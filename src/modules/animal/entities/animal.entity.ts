import { BreedEntity } from 'src/modules/breed/entities/breed.entity';
import { SpecieEntity } from 'src/modules/specie/entities/specie.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
@Entity('animal')
export class AnimalEntity {
  @PrimaryGeneratedColumn('uuid')
  id?: string;
  @Column({ default: '' })
  name: string;
  @Column()
  weight: number;
  @Column({ default: '#' })
  record: string;
  @Column({
    type: 'enum',
    enum: ['male', 'female', 'undefined'],
    default: 'undefined',
  })
  gender: string;
  @Column()
  hair_color: string;
  @Column({ nullable: true })
  birthday: Date;
  @ManyToOne(() => BreedEntity, (breed) => breed.id, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  breed: BreedEntity;
  @ManyToOne(() => SpecieEntity, (specie) => specie.id, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  specie: SpecieEntity;
  @CreateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  created_at?: Date;
  @UpdateDateColumn({ type: 'timestamptz', default: () => 'CURRENT_TIMESTAMP' })
  updated_at?: Date;
}
