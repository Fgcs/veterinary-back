import { forwardRef, Module } from '@nestjs/common';
import { AnimalService } from './animal.service';
import { AnimalController } from './animal.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AnimalEntity } from './entities/animal.entity';
import { BreedModule } from '../breed/breed.module';
import { SpecieModule } from '../specie/specie.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([AnimalEntity]),
    forwardRef(() => BreedModule),
    forwardRef(() => SpecieModule),
  ],
  controllers: [AnimalController],
  providers: [AnimalService],
  exports: [AnimalService],
})
export class AnimalModule {}
