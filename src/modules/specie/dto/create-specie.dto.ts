import { IsNotEmpty, IsOptional } from 'class-validator';

export class CreateSpecieDto {
  @IsNotEmpty()
  name: string;
  @IsOptional()
  description: string;
}
