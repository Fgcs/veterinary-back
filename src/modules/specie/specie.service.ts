import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSpecieDto } from './dto/create-specie.dto';
import { UpdateSpecieDto } from './dto/update-specie.dto';
import { SpecieEntity } from './entities/specie.entity';

@Injectable()
export class SpecieService {
  constructor(
    @InjectRepository(SpecieEntity)
    private specieDB: Repository<SpecieEntity>,
  ) {}
  async getAll(): Promise<SpecieEntity[]> {
    return await this.specieDB.find();
  }
  async getbyId(id: string): Promise<SpecieEntity> {
    return await this.specieDB.findOne(id).catch((err) => {
      throw new HttpException('specie no found', HttpStatus.NOT_FOUND);
    });
  }
  async create(specie: CreateSpecieDto): Promise<SpecieEntity> {
    specie.name = specie.name.toLowerCase().trim();

    return await this.specieDB.save(specie).catch((err) => {
      throw new HttpException('create specie error', HttpStatus.NOT_FOUND);
    });
  }
  async update(specie: UpdateSpecieDto, id: string): Promise<SpecieEntity> {
    specie.name = specie.name.toLowerCase().trim();
    const dataUpdate = await this.specieDB.update(id, specie);
    return await this.specieDB.findOne(id);
  }
  async delete(id: string): Promise<SpecieEntity> {
    const specie = await this.specieDB.findOne(id);
    this.specieDB.delete(id);
    return specie;
  }
  async foundName(name: string): Promise<SpecieEntity[]> {
    name = name.toLowerCase();
    return await this.specieDB
      .createQueryBuilder('specie')

      .where('specie.name like :name', {
        name: `%${name}%`,
      })
      .select(['specie.name', 'specie.id'])
      .getMany();
    // this.specieDB.find({
    //   select: ['id', 'name'],
    //   where: [{ name: `%${name}%` }],
    // });
  }
}
