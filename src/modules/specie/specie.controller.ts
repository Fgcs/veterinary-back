import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { SpecieService } from './specie.service';
import { CreateSpecieDto } from './dto/create-specie.dto';
import { UpdateSpecieDto } from './dto/update-specie.dto';
import { ApiTags } from '@nestjs/swagger';
import { SpecieEntity } from './entities/specie.entity';
@ApiTags('Species')
@Controller('api/specie')
export class SpecieController {
  constructor(private readonly specieService: SpecieService) {}

  @Post()
  create(@Body() createSpecieDto: CreateSpecieDto): Promise<SpecieEntity> {
    return this.specieService.create(createSpecieDto);
  }

  @Get()
  findAll(): Promise<SpecieEntity[]> {
    return this.specieService.getAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string): Promise<SpecieEntity> {
    return this.specieService.getbyId(id);
  }
  @Get('search/find-by-name')
  async findByEmail(@Query('name') name: string) {
    return await this.specieService.foundName(name);
  }
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateSpecieDto: UpdateSpecieDto,
  ): Promise<SpecieEntity> {
    return this.specieService.update(updateSpecieDto, id);
  }

  @Delete(':id')
  remove(@Param('id') id: string): Promise<SpecieEntity> {
    return this.specieService.delete(id);
  }
}
