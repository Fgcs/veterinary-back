import ShortUniqueId from 'short-unique-id';
const uid = new ShortUniqueId();

export const skuGenerate = () => uid.stamp(15);
