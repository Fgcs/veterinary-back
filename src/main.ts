import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  SwaggerModule,
  DocumentBuilder,
  SwaggerCustomOptions,
} from '@nestjs/swagger';
import { NestExpressApplication } from '@nestjs/platform-express';
import { ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { setDefaultUser } from './modules/user/config/user-default';
async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.enableCors();
  app.useGlobalPipes(new ValidationPipe());
  const options = new DocumentBuilder()
    .addBearerAuth()
    .setTitle('La Esperanza - Api')
    .setDescription('API - La Esperanza Servicios Exequiales.CA')
    .setVersion('0.1')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  const customOptions: SwaggerCustomOptions = {
    swaggerOptions: {
      persistAuthorization: true,
    },
    customSiteTitle: 'My API Docs',
  };
  const configService = app.get(ConfigService);
  setDefaultUser(configService);
  SwaggerModule.setup('api', app, document, customOptions);
  await app.listen(process.env.PORT || 3000, () => {
    console.log(`server run in the port ${process.env.PORT}`);
  });
}
bootstrap();
